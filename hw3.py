# ---------------------------------------------------- #
# File: hw3.py
# ---------------------------------------------------- #
# Authors: Clement Rames & Bradley Caponigro
# [https://bitbucket.org/clementrames/hw3]
# ---------------------------------------------------- #
# Plaftorm:    {Windows | Unix (includes Mac OS)}
# Environment: Python 2.7.8
# Libaries:    numpy 1.9.0
#              scipy
#              matplotlib 1.4.0
#              Tkinter
# ---------------------------------------------------- #
# Description:
'''This program simulates a 1 or 2 link pendulum. Please select the number
of links desired as well as the variable to be plotted in the menu bar, 
and adjust the parameters using the sliders. Press plot to display result 
and quit to exit the application.'''
# ---------------------------------------------------- #
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import numpy as np
from numpy import sin, cos, pi
import scipy.integrate as integrate
import Tkinter as tk

TITLE_FONT = ("Helvetica", 10)
# ------------------------
class PendulumGUI(tk.Frame):
    def __init__(self, master = None):
        tk.Frame.__init__(self, master)
        self.master.title('Pendulum Simulation')
        self._current_column_slider = 1
        self._current_column_button = 1
        self.sliders = {}
        self.entries = {}
        self.G = 9.81
        self.n = 2
        #create a menubar
        self.menubar = tk.Menu(root)
        #Create the 2 drop down menus
        self.add_menu('Number of links', '1', '2', self.one_link, self.two_links)
        self.add_menu('Variable to plot', 'x', 'y', self.plot_x, self.plot_y)
        self.create_widgets()
        self.grid()
        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(0, weight=1)
        self.fig = plt.figure()
        self.font = ("Helvetica", 18, "bold")
            
    def create_widgets(self):
        #Parameters for the simulation
        self.add_slider('M1', (0, 10), 1)
        self.add_slider('L1', (0, 5), 1)
        self.add_slider('th1', (0, 180), 120)
        self.add_slider('om1', (-10, 10), 0)
        if self.n == 2:
            self.add_slider('M2', (0, 10), 1)
            self.add_slider('L2', (0, 5), 1)
            self.add_slider('th2', (0, 180), -10)
            self.add_slider('om2', (-10, 10), 0)
        
        #Create buttons for the GUI
        self.add_button("Plot", self.plot)
        self.add_button("Quit", self.quit)
        
        #Create entry boxes for the GUI
        self.add_entry('t', 20)
        self.add_entry('dt', 0.1)
    
    def add_slider(self, label, bounds, default):
        new_label = tk.Label(self, text=label, font=TITLE_FONT)
        new_label.grid(column=self._current_column_slider, row=1)
        resolution = (bounds[1] - bounds[0]) / 100.0
        new_parameter = tk.Scale(self, from_=bounds[0], to=bounds[1], resolution=resolution)
        new_parameter.grid(column=self._current_column_slider, row=2)
        new_parameter.set(default)
        self.sliders[label] = new_parameter
        self._current_column_slider += 1
        
    def add_button(self, text, command):
        new_button = tk.Button(self, text = text, command = command)
        new_button.grid(column = self._current_column_button, row = 0)
        self._current_column_button += 1
        
    def add_entry(self, text, default):
        new_label = tk.Label(self, text=text, font=TITLE_FONT)
        new_label.grid(column = self._current_column_button, row = 0)
        textvariable = tk.StringVar()
        new_entry = tk.Entry(self,textvariable=textvariable)
        new_entry.grid(column=self._current_column_button+1,row=0)
        textvariable.set(default)
        self.entries[text] = textvariable.get()
        self._current_column_button += 1
        
    def add_menu(self, label, option1, option2, command1, command2):
        #add drop down ro menubar
        dropdown = tk.Menu(self.menubar, tearoff=0)
        #title dropdown
        self.menubar.add_cascade(label=label, menu=dropdown)
        #add components to dropdown
        dropdown.add_command(label=option1, command=command1)
        dropdown.add_command(label=option2, command=command2)
        # display the menu
        root.config(menu=self.menubar)
        
    def one_link(self):
        self.n = 1
        return self.n
        
    def two_links(self):
        self.n = 2
        return self.n
        
    def plot_x(self):
        #create subplot for graph
        bx = self.fig.add_subplot(122, autoscale_on=False, xlim=(0, int(self.entries['t'])),
                                  ylim=(-10, 10))
        bx.grid()
        bx.set_title('X position (m) vs time (sec)')
        bx.plot(self.t, self.x1)
        if self.n == 2:
            bx.plot(self.t, self.x2)

    def plot_y(self):
        #create subplot for graph
        by = self.fig.add_subplot(122, autoscale_on=False, xlim=(0, int(self.entries['t'])), 
                                  ylim=(-10, 10))
        by.grid()
        by.set_title('Y position (m) vs time (sec)')
        by.plot(self.t, self.y1)
        if self.n == 2:
            by.plot(self.t, self.y2)
        
        
    def plot(self):
        #this method takes care of the plotting of the pendulum
        self.fig.clear()
        self.parameters = {label: slider.get() for label, slider in self.sliders.items()}
        
        canvas = FigureCanvasTkAgg(self.fig, master=self)
        canvas.get_tk_widget().grid(column=0, row= 10, columnspan=20)
        lmax = self.parameters['L1'] + self.parameters['L2']    
        ax = self.fig.add_subplot(121, autoscale_on=False, xlim=(-lmax, lmax), ylim=(-lmax, lmax))
        ax.grid()
    
        self.line, = ax.plot([], [], 'o-', lw=2)
        self.line.set_data([], [])
    
        self.time_template = 'time = %.1fs'
        self.time_text = ax.text(0.05, 0.9, '', transform=ax.transAxes)
        self.time_text.set_text('')
        
        self.dt = float(self.entries['dt'])
        self.t = np.arange(0.0, int(self.entries['t']), self.dt)
        
        self.solve(self.t)
        self.ani = animation.FuncAnimation(self.fig, self.animate, np.arange(1, len(self.y)),
                                      interval=25, blit=False, init_func=self.init, repeat=True)
        canvas.show()
               
    def init(self):
        #initialises the animation with an empty plot
        return self.line, self.time_text
    
    def animate(self,i):
        #this method iterates through all values to animate the pendulum
        if self.n == 1:
            self.thisx = [0,self.x1[i]]
            self.thisy = [0, self.y1[i]]
            self.line.set_data(self.thisx, self.thisy)
            self.time_text.set_text(self.time_template%(i*self.dt))
            return self.line, self.time_text
        elif self.n == 2:
            self.thisx = [0,self.x1[i], self.x2[i]]
            self.thisy = [0, self.y1[i], self.y2[i]]
            self.line.set_data(self.thisx, self.thisy)
            self.time_text.set_text(self.time_template%(i*self.dt))
            return self.line, self.time_text
        
    def solve(self, t):
        
        self.dt = float(self.entries['dt'])
        self.t = np.arange(0.0, int(self.entries['t']), self.dt)              
        if self.n==1:
            # initial state
            state = np.array([self.parameters['th1'], self.parameters['om1']])*pi/180
        
            # integrate ODE using odeint
            self.y = integrate.odeint(self.physics, state, self.t)
        
            self.x1 = self.parameters['L1']*sin(self.y[:,0])
            self.y1 = -self.parameters['L1']*cos(self.y[:,0])        
        elif self.n==2:
            # initial state
            state = np.array([self.parameters['th1'], self.parameters['om1'], 
                              self.parameters['th2'], self.parameters['om2']])*pi/180
        
            # integrate ODE using odeint
            self.y = integrate.odeint(self.physics, state, self.t)
            
            #coordinates of the first mass
            self.x1 = self.parameters['L1']*sin(self.y[:,0])
            self.y1 = -self.parameters['L1']*cos(self.y[:,0])
            
            #coordinates of the second mass
            self.x2 = self.parameters['L2']*sin(self.y[:,2]) + self.x1
            self.y2 = self.parameters['L2']*cos(self.y[:,2]) + self.y1
        
    def physics(self, state, t, p=None):
        # this method represents the 2-link pendulum
        p = self.parameters
        
        if self.n == 1:
            dydx = np.zeros_like(state)
            dydx[0] = state[1]
            dydx[1] = -(2*self.G/p['L1'])*np.sin(state[0]) - (1/p['M1'])*state[1]
            return dydx
            
        
        elif self.n == 2:
            dydx = np.zeros_like(state)
            dydx[0] = state[1]
            del_ = state[2]-state[0]
            den1 = (p['M1']+p['M2'])*p['L1'] - p['M2']*p['L1']*cos(del_)*cos(del_)
            dydx[1] = (p['M2']*p['L1']*state[1]*state[1]*sin(del_)*cos(del_)
                       + p['M2']*self.G*sin(state[2])*cos(del_) + p['M2']*p['L2']*state[3]*state[3]*sin(del_)
                       - (p['M1']+p['M2'])*self.G*sin(state[0]))/den1
        
            dydx[2] = state[3]
        
            den2 = (p['L2']/p['L1'])*den1
            dydx[3] = (-p['M2']*p['L2']*state[3]*state[3]*sin(del_)*cos(del_)
                       + (p['M1']+p['M2'])*self.G*sin(state[0])*cos(del_)
                       - (p['M1']+p['M2'])*p['L1']*state[1]*state[1]*sin(del_)
                       - (p['M1']+p['M2'])*self.G*sin(state[2]))/den2
            return dydx
        
# ------------------------
# END-OF-Class:->PendulumGUI<-
            
root = tk.Tk()    
app = PendulumGUI(master = root)
app.mainloop()
root.destroy()

# ---------------------------------------------------- #
# END-OF-File:->hw3.py<-
# ---------------------------------------------------- #